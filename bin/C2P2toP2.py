#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 15:36:56 2020

@author: beilin
"""

import gnsstoolbox.rinex_o as rx
import sys, getopt
import os

def main(argv):
    inputfile = ''
    outputfile = ''
    
    try:
        opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
    except getopt.GetoptError:
        print('C2P2toP2.py -i <inputfile> -o <outputfile>')
        sys.exit(2)
        
    for opt, arg in opts:
        if opt == '-h':
            print('C2P2toP2.py -i <inputfile> -o <outputfile>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
            
    if os.path.exists(inputfile):
        rnx = rx.rinex_o()
        rnx.loadRinexO(inputfile)      
        rnx.fillP2withC2()    
        parameters={"OBSERVABLES":['C1', 'P2', 'L1', 'L2'], "CONST":'GR'}
        rnx.writeRinex2(outputfile, parameters)

if __name__ == "__main__":
    if len(sys.argv) > 0:
        main(sys.argv[1:])
    else:
        print('C2P2toP2.py -i <inputfile> -o <outputfile>')