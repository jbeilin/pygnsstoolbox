gnsstoolbox - Python package for GNSS learning and research
===========================================================

# Usage

For usage see gnsstoolbox cheatsheet and beamer slides shipped with the package. 

# Dependencies

This package requires Python 3.0

Required packages 
* gpsdatetime - https://pypi.org/project/gpsdatetime/ (J. Beilin - ENSG)
* re
* math
* numpy
* copy
* time
* os
* from operator import attrgetter

The Generic Mapping Tools should be installed for skyplots. 

# Installation

Installation is accomplished from the command line.

## From pypi

user@desktop:pip3 install pygnsstoolbox

## From package 

user@desktop:~/pygnsstoolbox$ python3 setup.py install

The above command needs to be performed as root.

# licence

Copyright (C) 2014-2023, Jacques Beilin / ENSG-Geomatique

Distributed under terms of the CECILL-C licence.
